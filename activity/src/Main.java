import com.zuitt.example.*;

import java.util.ArrayList;


public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");

        phonebook.setContact(contact1);
        phonebook.setContact(contact2);

        System.out.println(phonebook.getContact());

        ArrayList<Contact> contacts = phonebook.getContact();
        if (contacts.isEmpty()){
            System.out.println("Phonebook is empty..");
        } else {
            for (Contact contact : contacts){
                System.out.println(contact.getName());
                System.out.println("-------------------");
                System.out.println(contact.getName() + " has the following registered number(s): ");
                System.out.println(contact.getContactNumber());
                System.out.println("-------------------");
                System.out.println(contact.getName() + " has the following registered address(es): ");
                System.out.println("My home is in " + contact.getAddress());
                System.out.println("==========================");

            }
        }

    }
}