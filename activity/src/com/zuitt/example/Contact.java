package com.zuitt.example;

public class Contact {

    private String name, contactNumber, address;

    public Contact(){
        this.name = "Whats Name?";
        this.contactNumber = "+001234567890";
        this.address = "New Home";
    }

    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
